import React, { Component } from 'react'
import './App.css'
import Navbar from './components/Navbar'
import Home from './components/Home'
import Create from './components/Create'
import Update from './components/Update'
import { BrowserRouter as Router, Route } from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Navbar />
          <div className="container">
            <Route exact path="/" component={Home} />
            <Route exact path="/create" component={Create} />
            <Route exact path="/update/:id" component={Update} />
          </div>
        </div>
      </Router>
    )
  }
}

export default App
