import React, { Component } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import { confirmAlert } from 'react-confirm-alert'
import 'react-confirm-alert/src/react-confirm-alert.css'

class Dap extends Component {
  onClick = e => {
    e.preventDefault()
    let id = e.target.parentNode.parentNode.id
    console.log(id)
    confirmAlert({
      title: 'Confirmação',
      message: 'Você tem certeza que deseja apagar esse agricultor?',
      buttons: [
        {
          label: 'Sim',
          onClick: () => this.onDelete(id)
        },
        {
          label: 'Não',
          onClick: () => alert('Operação cancelada.')
        }
      ]
    })
  }

  onDelete = id => {
    let url = `/api/dap/${id}`
    axios.delete(url)
    window.location.reload()
  }

  render() {
    return (
      <div className="card" style={{ width: 18 + 'rem' }} id={this.props.id}>
        <div className="card-body">
          <h5 className="card-title">{this.props.dados.nome}</h5>
          <h6 className="card-subtitle mb-2 text-muted">
            {this.props.dados.classificacao}
          </h6>
          <p className="card-text">
            <strong>Planta: </strong>
            {this.props.dados.planta.join(',')}
          </p>
          <p className="card-text">
            <strong>Vende: </strong>
            {this.props.dados.vende.join(',')}
          </p>
        </div>
        <ul className="list-group list-group-flush">
          <li className="list-group-item">
            <strong>Área: </strong>
            {this.props.dados.area} hectares
          </li>
          <li className="list-group-item">
            <strong>Endereço: </strong>
            {this.props.dados.endereco}
          </li>
          <li className="list-group-item">
            <strong>CPF: </strong>
            {this.props.dados.cpf}
          </li>
        </ul>
        <div className="card-body">
          <Link to={`update/${this.props.id}`} className="card-link">
            Atualizar
          </Link>
          <a href="/" className="card-link" onClick={this.onClick}>
            Deletar
          </a>
        </div>
      </div>
    )
  }
}

export default Dap
