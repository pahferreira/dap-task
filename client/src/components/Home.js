import React, { Component } from 'react'
import Dap from './Dap'
import axios from 'axios'

class Home extends Component {
  constructor() {
    super()
    this.state = {}
  }

  componentDidMount() {
    axios.get('/api/dap/all').then(response => {
      let data = response.data.results
      this.setState({ data })
    })
  }

  render() {
    return (
      <div className="card-columns">
        {!this.state.data ? (
          <em>Loading...</em>
        ) : (
          this.state.data.map(agricultor => {
            return (
              <Dap
                key={agricultor._id}
                dados={agricultor}
                id={agricultor._id}
              />
            )
          })
        )}
      </div>
    )
  }
}

export default Home
