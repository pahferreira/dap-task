import React, { Component } from 'react'
import InputNome from './form/InputNome'
import InputCpf from './form/InputCpf'
import InputEndereco from './form/InputEndereco'
import InputArea from './form/InputArea'
import SelectClassificacao from './form/SelectClassificacao'
import Planta from './form/Planta'
import Vende from './form/Vende'
import axios from 'axios'

class Update extends Component {
  constructor() {
    super()
    this.state = {
      nome: '',
      cpf: '',
      area: '',
      classificacao: '',
      endereco: '',
      planta: [],
      vende: [],
      loading: true
    }
  }

  async componentDidMount() {
    let url = `/api/dap/${this.props.match.params.id}`
    try {
      let response = await axios.get(url)
      this.setState({ ...response.data })
      this.setState({ loading: false })
    } catch (err) {
      console.log(err)
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value })
  }

  onSubmit = e => {
    e.preventDefault()
    let url = `/api/dap/${this.props.match.params.id}`
    let agricultor = {
      nome: this.state.nome,
      cpf: this.state.cpf,
      area: this.state.area,
      classificacao: this.state.classificacao,
      endereco: this.state.endereco,
      planta: this.state.planta,
      vende: this.state.vende
    }
    console.log(agricultor.classificacao)
    axios.post(url, agricultor)
    window.alert('Agricultor atualizado com sucesso.')
    document.location.href = '/'
  }

  onPlantaCheck = e => {
    let opcao = e.target.value
    if (this.state.planta.includes(opcao)) {
      this.setState({
        planta: this.state.planta.filter(cultura => cultura !== opcao)
      })
    } else {
      this.setState({ planta: [...this.state.planta, opcao] })
    }
  }

  onVendeCheck = e => {
    let opcao = e.target.value
    if (this.state.vende.includes(opcao)) {
      this.setState({
        vende: this.state.vende.filter(cultura => cultura !== opcao)
      })
    } else {
      this.setState({ vende: [...this.state.vende, opcao] })
    }
  }

  onSelect = e => {
    this.setState({ classificacao: e.target.value })
  }

  render() {
    return (
      <div className="register">
        {this.state.loading ? (
          <em>Loading...</em>
        ) : (
          <div className="container">
            <div className="row">
              <div className="col-md-8 m-auto">
                <h1 className="display-4 text-center">Atualizar DAP</h1>
                <p className="lead text-center">
                  Atualizar DAP no Projeto Agricultura Familiar
                </p>
                <form onSubmit={this.onSubmit}>
                  <InputNome nome={this.state.nome} onChange={this.onChange} />
                  <InputCpf cpf={this.state.cpf} onChange={this.onChange} />
                  <InputEndereco
                    endereco={this.state.endereco}
                    onChange={this.onChange}
                  />
                  <InputArea area={this.state.area} onChange={this.onChange} />
                  <SelectClassificacao
                    value={this.state.classificacao}
                    onSelect={this.onSelect}
                  />
                  <Planta
                    selecionados={this.state.planta}
                    onPlantaCheck={this.onPlantaCheck}
                  />
                  <Vende
                    selecionados={this.state.vende}
                    onVendeCheck={this.onVendeCheck}
                  />
                  <input
                    type="submit"
                    className="btn btn-info btn-block mt-4"
                  />
                </form>
              </div>
            </div>
          </div>
        )}
      </div>
    )
  }
}

export default Update
