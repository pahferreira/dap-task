import React from 'react'

const InputArea = props => {
  return (
    <div className="form-group">
      <input
        type="text"
        className="form-control form-control-lg"
        placeholder="área"
        name="area"
        value={props.area}
        onChange={props.onChange}
      />
    </div>
  )
}

export default InputArea
