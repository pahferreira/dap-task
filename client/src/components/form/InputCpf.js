import React from 'react'

const InputCpf = props => {
  return (
    <div className="form-group">
      <input
        type="text"
        className="form-control form-control-lg"
        placeholder="CPF"
        name="cpf"
        value={props.cpf}
        onChange={props.onChange}
      />
    </div>
  )
}

export default InputCpf
