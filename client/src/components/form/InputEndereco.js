import React from 'react'

const InputEndereco = props => {
  return (
    <div className="form-group">
      <input
        type="text"
        className="form-control form-control-lg"
        placeholder="endereco"
        name="endereco"
        value={props.endereco}
        onChange={props.onChange}
      />
    </div>
  )
}

export default InputEndereco
