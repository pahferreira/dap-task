import React from 'react'

const InputNome = props => {
  return (
    <div className="form-group">
      <input
        type="text"
        className="form-control form-control-lg"
        placeholder="Nome"
        name="nome"
        value={props.nome}
        onChange={props.onChange}
      />
    </div>
  )
}

export default InputNome
