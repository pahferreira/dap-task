import React, { Component } from 'react'

class Planta extends Component {
  componentDidMount() {
    let checkboxes = document.querySelectorAll('.planta-check')
    checkboxes.forEach(checkbox => {
      if (this.props.selecionados.includes(checkbox.value)) {
        checkbox.setAttribute('checked', true)
      }
    })
  }

  render() {
    let culturas = ['arroz', 'feijão', 'mandioca', 'milho', 'algodão']
    return (
      <React.Fragment>
        <div className="form-group">
          <h5>O que é plantado pelo agricultor?</h5>
        </div>
        <div className="form-group">
          {culturas.map(cultura => {
            return (
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input planta-check"
                  type="checkbox"
                  value={cultura}
                  onClick={this.props.onPlantaCheck}
                  id={`${cultura}Check`}
                />
                <label className="form-check-label" htmlFor="arrozCheck">
                  {cultura.charAt(0).toUpperCase() + cultura.slice(1)}
                </label>
              </div>
            )
          })}
        </div>
      </React.Fragment>
    )
  }
}

export default Planta
