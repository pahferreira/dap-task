import React from 'react'

const SelectClassificacao = props => {
  return (
    <div className="form-group">
      <select
        className="custom-select"
        id="classificacaoSelect"
        value={props.classificacao}
        onChange={props.onSelect}
      >
        <option value="Dono">Dono</option>
        <option value="Arrendador">Arrendador</option>
        <option value="Meeiro">Meeiro</option>
        <option value="Prestador de serviço">Prestador de Serviço</option>
      </select>
    </div>
  )
}

export default SelectClassificacao
