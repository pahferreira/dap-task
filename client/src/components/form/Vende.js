import React, { Component } from 'react'

class Vende extends Component {
  componentDidMount() {
    let checkboxes = document.querySelectorAll('.vende-check')
    checkboxes.forEach(checkbox => {
      if (this.props.selecionados.includes(checkbox.value)) {
        checkbox.setAttribute('checked', true)
      }
    })
  }

  render() {
    let culturas = ['arroz', 'feijão', 'mandioca', 'milho', 'algodão']
    return (
      <React.Fragment>
        <div className="form-group">
          <h5>O que é vendido pelo agricultor?</h5>
        </div>
        <div className="form-group">
          {culturas.map(cultura => {
            return (
              <div className="form-check form-check-inline">
                <input
                  className="form-check-input vende-check"
                  type="checkbox"
                  value={cultura}
                  onClick={this.props.onVendeCheck}
                  id={`${cultura}VCheck`}
                />
                <label className="form-check-label" htmlFor="arrozCheck">
                  {cultura.charAt(0).toUpperCase() + cultura.slice(1)}
                </label>
              </div>
            )
          })}
        </div>
      </React.Fragment>
    )
  }
}

export default Vende
