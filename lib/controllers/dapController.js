import Dap from '../db/models/Dap'

const readAll = (req, res) => {
  Dap.find().then(results => {
    res.json({ results })
  })
}

const readById = (req, res) => {
  Dap.findById(req.params.id).then(agricultor => {
    if (agricultor) {
      res.json(agricultor)
    } else {
      res.status(404).json({ error: 'Agricultor não encontrado.' })
    }
  })
}

const createOne = (req, res) => {
  Dap.findOne({ cpf: req.body.cpf }).then(agricultor => {
    if (agricultor) {
      res.status(400).json({ error: 'CPF já cadastrado.' })
    } else {
      let novoAgricultor = new Dap({
        nome: req.body.nome,
        cpf: req.body.cpf,
        classificacao: req.body.classificacao,
        area: req.body.area,
        endereco: req.body.endereco,
        planta: req.body.planta,
        vende: req.body.vende
      })
      novoAgricultor
        .save()
        .then(novoAgricultor => res.json(novoAgricultor))
        .catch(err => console.log(err))
    }
  })
}

const updateOne = (req, res) => {
  let novosDados = {}
  if (req.body.nome) novosDados.nome = req.body.nome
  if (req.body.cpf) novosDados.cpf = req.body.cpf
  if (req.body.area) novosDados.area = req.body.area
  if (req.body.endereco) novosDados.endereco = req.body.endereco
  if (req.body.classificacao) novosDados.classificacao = req.body.classificacao
  if (req.body.planta) novosDados.planta = req.body.planta
  if (req.body.vende) novosDados.vende = req.body.vende

  Dap.findByIdAndUpdate(
    req.params.id,
    { $set: novosDados },
    { new: true }
  ).then(agricultor => {
    res.json(agricultor)
  })
}

const deleteOne = (req, res) => {
  Dap.findByIdAndDelete(req.params.id).then(agricultorDeletado => {
    res.json({ sucess: true, infos: agricultorDeletado })
  })
}

export default { readAll, readById, createOne, updateOne, deleteOne }
