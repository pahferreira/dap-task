import mongoose from 'mongoose'

const conectarDB = () => {
  mongoose
    .connect(
      'mongodb://localhost/dap',
      { useNewUrlParser: true }
    )
    .then(() => console.log('Banco de Dados Conectado.'))
    .catch(err => console.log(err))
  // mongoose
  // .connect(
  //   'mongodb://admin:admin123@ds221242.mlab.com:21242/dap-test',
  //   { useNewUrlParser: true }
  // )
  // .then(() => console.log('Banco de Dados Conectado.'))
  // .catch(err => console.log(err))
}

export default { conectarDB }
