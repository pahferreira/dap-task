import mongoose from 'mongoose'
let Schema = mongoose.Schema

let DapSchema = new Schema({
  nome: {
    type: String,
    required: true
  },
  cpf: {
    type: String,
    required: true
  },
  classificacao: {
    type: String,
    required: true
  },
  area: {
    type: Number
  },
  planta: {
    type: [String]
  },
  vende: {
    type: [String]
  },
  endereco: {
    type: String
  }
})

export default mongoose.model('dap', DapSchema)
