import express from 'express'
import dapController from '../../controllers/dapController'

let router = express.Router()

router.get('/all', dapController.readAll)
router.get('/:id', dapController.readById)
router.post('/create', dapController.createOne)
router.post('/:id', dapController.updateOne)
router.delete('/:id', dapController.deleteOne)

export default router
