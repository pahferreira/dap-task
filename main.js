import express from 'express'
import db from './lib/db/index'
import bodyParser from 'body-parser'
import dapRouter from './lib/routes/api/index'

let app = express()

//Body Parser para requisições POST através de Forms HTML
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//Conexão com Banco de Dados
db.conectarDB()

//Rotas
app.use('/api/dap', dapRouter)

app.listen(5000, () => console.log('Servidor inicializado.'))
